#Project info
author=Zeitheron
start_git_commit=?
git_commit=https://gitlab.com/Zeitheron/???/commit/{{hash}}

#Runtime info
forge_version=14.23.5.2838
minecraft_version=1.12.2
mcp=stable_39
hc_version=2.0.6.1
jei_version=4.11.0.206

#Mod info
mod_id=hcmb
mod_id_fancy=HammerMicroblocks
mod_name=Hammer Microblocks
mod_version=12.2.1
cf_project=267263
release_type=release